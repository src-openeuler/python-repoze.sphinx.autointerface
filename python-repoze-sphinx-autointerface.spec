%global _empty_manifest_terminate_build 0
Name:           python-repoze-sphinx-autointerface
Version:        1.0.0
Release:        1
Summary:        Sphinx extension: auto-generates API docs from Zope interfaces
License:        BSD
URL:            http://www.repoze.org
Source0:        https://files.pythonhosted.org/packages/e2/c3/5a8238e9fcba69e3a925d8a4e88547765e110f807bab47c56598d7aa9a5b/repoze.sphinx.autointerface-1.0.0.tar.gz
Requires:       python3-Sphinx
Requires:       python3-setuptools

BuildArch:      noarch
%description
Sphinx extension: auto-generates API docs from Zope interfaces

%package -n python3-repoze-sphinx-autointerface
Summary:        Sphinx extension: auto-generates API docs from Zope interfaces
Provides:       python-repoze-sphinx-autointerface
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pip
%description -n python3-repoze-sphinx-autointerface
Sphinx extension: auto-generates API docs from Zope interfaces

%package help
Summary:        Sphinx extension: auto-generates API docs from Zope interfaces
Provides:       python3-repoze-sphinx-autointerface-doc
%description help
Sphinx extension: auto-generates API docs from Zope interfaces

%prep
%autosetup -n repoze.sphinx.autointerface-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-repoze-sphinx-autointerface -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Jul 28 2022 liqiuyu <liqiuyu@kylinos.cn> - 1.0.0-1
- update to 1.0.0

* Tue Aug 17 2021 OpenStack_SIG <openstack@openeuler.org> - 0.8-1
- Package Spec generate
